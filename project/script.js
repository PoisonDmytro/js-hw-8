// Опишіть своїми словами що таке Document Object Model (DOM)
// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// Це структура дерева елементів сторінки, допомогає взаємодії Html js i css

//InnerHTML повертає весь текст, включаючи теги HTML, який міститься в елементі.
//innerText повертає весь текст і теги, що містяться в елементі та всіх його дочірніх елементах.

//getElementById,getElementbyClassName,querySelector. querySelector - кращий

//----
serchPTonewBG('#ff0000')
serchFoId()
redactclassP()
getHeaderLi() 
removeClass('section-title')
//----
function serchPTonewBG(color){
    let allP = document.querySelectorAll('p');
    for(pElement of allP){
        pElement.style.backgroundColor = color;
    }
}
function serchFoId(){
    let IdElement = document.getElementById("optionsList");
    console.log("element");
    console.log(IdElement);
    console.log("parent");
    console.log(IdElement.parentNode);
    console.log("childs");
    for(i=0;i<IdElement.childNodes.length;i++){
        console.log(`child ${i+1}`);
        console.log(IdElement.childNodes[i]);
    }
}
function redactclassP() { //такого елементу немаэ на сторінці
    let testP = document.createElement("p");
    testP.classList.add("testParagraph");    //тому я створив такий елемент і запхнув у body
    document.body.append(testP);

    let classElement = document.querySelector('.testParagraph');
    console.log(classElement);
    classElement.innerText = "This is a paragraph";
}
function getHeaderLi() {
    let headerLi = document.querySelectorAll('.main-header > div > ul > li')
    for(liElement of headerLi){
        liElement.classList.add("nav-item");
        console.log(liElement);
    }
}
function removeClass(className){
    let classNameElements = document.querySelectorAll(`.${className}`);
    for(element of classNameElements){
        element.classList.remove(`${className}`);
        console.log(element);
    }
}